/* File: gulpfile.js */

// grab our gulp packages
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin');

gulp.task('default', ['build-js', 'build-css']);

// Minify Custom JS: Run manually with: "gulp build-js"
gulp.task('build-js', function () {
    return gulp.src(['public/js/vendor/jquery-1.12.0.min.js',
                     'public/js/vendor/tether.min.js',
    	               'node_modules/bootstrap/dist/js/bootstrap.js',
    								 'public/js/plugins.js',
    								 'public/js/main.js'
                   ])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(uglify({
            output: {
                'ascii_only': true
            }
        }))
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('public/assets/js'));
});

// sass to CSS: Run manually with: "gulp build-css"
gulp.task('build-css', function () {
		gulp.src(['node_modules/font-awesome/fonts/**/*'])
		    .pipe(gulp.dest('public/assets/fonts'));

    return gulp.src('public/scss/app.scss')
        .pipe(plumber())
        .pipe(sass())
        .on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        })
        .pipe(autoprefixer({
            browsers: [
                    '> 1%',
                    'last 2 versions',
                    'firefox >= 4',
                    'safari 7',
                    'safari 8',
                    'IE 8',
                    'IE 9',
                    'IE 10',
                    'IE 11'
                ],
            cascade: false
        }))
        // .pipe(cssmin())
        // .pipe(concat('style.min.css'))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('public/assets/css')).on('error', gutil.log);
});

// Default task
gulp.task('watch', function () {
    gulp.watch('public/js/**/*.js', ['build-js']);
    gulp.watch('public/scss/**/*.scss', ['build-css']);
});
